# Put this at the top of action.py
import socket

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 10000)
print ('connecting to %s port %s' % server_address, file=sys.stderr)
sock.connect(server_address)

class GetSentiment(object):

    """Gets the sentiment (postive or negative) of the command."""

    def __init__(self, say, keyword):
        self.say = say
        self.keyword = keyword

    def run(self, voice_command):
        
        sys.stdout.write(voice_command + '\n')
        
        #send the text to the neural network socket
        sock.sendall(bytes(voice_command,'ascii'))

        #receive the sentiment
        sentiment = sock.recv(1024)
        print (str(sentiment,'utf-8'), file=sys.stderr)
        self.say(str(sentiment,'utf-8'))
        
# Put this within make_actor method as last add_keyword as it is default
actor.add_keyword(_(''), GetSentiment(say, _('')))