import tensorflow as tf
import pickle
import numpy as np
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer
import socket
import sys


class MiaNeuralNet:
  lemmatizer = WordNetLemmatizer()

  n_nodes_hl1 = 500
  n_nodes_hl2 = 500

  n_classes = 2
  hm_data = 2000000

  batch_size = 32
  hm_epochs = 10

  x = tf.placeholder('float')
  y = tf.placeholder('float')


  current_epoch = tf.Variable(1)

  hidden_1_layer = {'f_fum':n_nodes_hl1,
                    'weight':tf.Variable(tf.random_normal([2638, n_nodes_hl1])),
                    'bias':tf.Variable(tf.random_normal([n_nodes_hl1]))}

  hidden_2_layer = {'f_fum':n_nodes_hl2,
                    'weight':tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])),
                    'bias':tf.Variable(tf.random_normal([n_nodes_hl2]))}

  output_layer = {'f_fum':None,
                  'weight':tf.Variable(tf.random_normal([n_nodes_hl2, n_classes])),
                  'bias':tf.Variable(tf.random_normal([n_classes])),}
  
  def neural_network_model(self,data):

    l1 = tf.add(tf.matmul(data,self.hidden_1_layer['weight']), self.hidden_1_layer['bias'])
    l1 = tf.nn.relu(l1)

    l2 = tf.add(tf.matmul(l1,self.hidden_2_layer['weight']), self.hidden_2_layer['bias'])
    l2 = tf.nn.relu(l2)

    output = tf.matmul(l2,self.output_layer['weight']) + self.output_layer['bias']

    return output
    
  saver = tf.train.Saver()
  
  def __init__(self):
    self.prediction = self.neural_network_model(self.x)
    with open('lexicon.pickle','rb') as f:
        self.lexicon = pickle.load(f)
        
    #with tf.Session() as sess:
    self.sess=tf.Session()
    self.sess.run(tf.global_variables_initializer())
    self.saver.restore(self.sess, "model.ckpt")
  
  def closeSession(self):
    self.sess.close()
    self.sess=None   
    
  def getSentiment(self,input_data):
    current_words = word_tokenize(input_data.lower())
    current_words = [self.lemmatizer.lemmatize(i) for i in current_words]
    features = np.zeros(len(self.lexicon))

    for word in current_words:
        if word.lower() in self.lexicon:
            index_value = self.lexicon.index(word.lower())
            features[index_value] += 1

    features = np.array(list(features))

    result = (self.sess.run(tf.argmax(self.prediction.eval(session=self.sess,feed_dict={self.x:[features]}),1)))
    return result[0]

if __name__ == '__main__':
    neu=MiaNeuralNet()
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', 10000)
    print ('starting up on %s port %s' % server_address, file=sys.stderr)
    sock.bind(server_address)
    
    # Listen for incoming connections
    sock.listen(1)

    while True:
      # Wait for a connection
      print ('waiting for a connection',file=sys.stderr)
      connection, client_address = sock.accept()
      try:
        print ('connection from', client_address, file=sys.stderr)
        sdata=''
        while True:
            data = connection.recv(1024)  
              
            if not data: break
            sentiment=neu.getSentiment(str(data,'utf-8'))
            #Return positive or negative to socket
            if sentiment==1:
              connection.sendall(b'Positive')
            else:
               connection.sendall(b'Negative')
            
      finally:
        # Clean up the connection
        connection.close()
        neu.closeSession()